# Proyecto base con Spring Boot 

## Herramientas

- Spring tool suite: https://spring.io/tools3/sts/all

## Configuración
- Importar el proyecto desde "Import/Existing Maven Project"
- En la sección "Run As/Run Configurations" agrega las siguientes variables de entorno:
	- URL: jdbc:oracle:thin:@HOST:PORT:SERVICE_NAME
	- USERNAME
	- PASSWORD

## USUARIO
-	Administrador: etolentino/12345678
-	Usuario: gchiarella/12345678

## URL
Consultar los endpoints: http://localhost:8080/swagger-ui.html

