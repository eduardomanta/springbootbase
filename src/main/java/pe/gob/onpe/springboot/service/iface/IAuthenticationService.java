package pe.gob.onpe.springboot.service.iface;

import pe.gob.onpe.springboot.dto.RenewPasswordFirstDTO;
import pe.gob.onpe.springboot.dto.RespuestaApi;
import pe.gob.onpe.springboot.dto.UpdatePasswordDTO;

public interface IAuthenticationService {
	public RespuestaApi getToken(String username, String password);
	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword);
	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword);
	public RespuestaApi signOut(String token);
	public RespuestaApi refreshToken(String token);
}
