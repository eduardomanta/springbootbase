package pe.gob.onpe.springboot.service.iface;

import java.util.List;

import pe.gob.onpe.springboot.model.Encuesta;

public interface IEncuestaService {
	List<Encuesta> list();
	void save(Encuesta e);
}
