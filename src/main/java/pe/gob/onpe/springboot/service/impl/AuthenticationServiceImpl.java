package pe.gob.onpe.springboot.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

import pe.gob.onpe.springboot.dao.iface.IUsuarioDao;
import pe.gob.onpe.springboot.dto.RenewPasswordFirstDTO;
import pe.gob.onpe.springboot.dto.RespuestaApi;
import pe.gob.onpe.springboot.dto.UpdatePasswordDTO;
import pe.gob.onpe.springboot.model.Usuario;
import pe.gob.onpe.springboot.service.iface.IAuthenticationService;

@Service
public class AuthenticationServiceImpl implements IAuthenticationService {
	protected Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	IUsuarioDao usuarioDao;

	@Override
	public RespuestaApi getToken(String username, String password) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");
		try {
			logger.debug(username);
			logger.debug(password);
			Usuario usuario = usuarioDao.login(username, password);
			if (usuario != null) {
				Algorithm algorithm = Algorithm.HMAC256("secret"); /*Generamos un Token con base a una contraseña*/
				String token = JWT.create().withClaim("name", usuario.getUsuario()).sign(algorithm);
				rpta.setStatus("OK");
				rpta.setAccessToken(token);
				rpta.setBody(usuario);
			} else {
				rpta.setBody("Usuario no autorizado");
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			logger.error(exception.getMessage());
			// Invalid Signing configuration / Couldn't convert Claims.
		}

		return rpta;
	}

	@Override
	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		Map<String, String> challengeResponses = new HashMap<String, String>();
		challengeResponses.put("USERNAME", updatePassword.getUsername());
		challengeResponses.put("NEW_PASSWORD", updatePassword.getPassword());


		

		return rpta;
	}

	@Override
	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		/*
		 * try {
		 * 
		 * ConfirmForgotPasswordRequest confirmForgotPasswordRequest = new
		 * ConfirmForgotPasswordRequest() .withClientId(cognitoClienteId)
		 * .withConfirmationCode(updatePassword.getVerificationCode())
		 * .withPassword(updatePassword.getNewPassword())
		 * .withUsername(updatePassword.getUsername());
		 * 
		 * cognitoClient.confirmForgotPassword(confirmForgotPasswordRequest);
		 * rpta.setStatus("OK"); rpta.setBody("Clave cambiada correctamente"); } catch
		 * (NotAuthorizedException e) { rpta.setBody("Usuario no autorizado"); } catch
		 * (UserNotFoundException e) { rpta.setBody("Usuario no autorizado"); }
		 * catch(Exception e) {
		 * logger.error("[updatePassword] Ocurrio un error inesperado: ", e);
		 * rpta.setBody("Ocurrio un error inespera     do"); }
		 */

		return rpta;
	}

	/**
	 * Se puede ingresar el accessToken o idToken
	 */
	@Override
	public RespuestaApi signOut(String token) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		/*
		 * try { GlobalSignOutRequest rq = new GlobalSignOutRequest()
		 * .withAccessToken(token); cognitoClient.globalSignOut(rq);
		 * rpta.setStatus("OK"); rpta.setBody("SignOut correcto"); }catch(Exception e) {
		 * logger.error("[signOut] Ocurrio un error inesperado: ", e);
		 * rpta.setBody(e.getMessage()); }
		 */

		return rpta;
	}

	/**
	 * Se necesita el refreshToken como input
	 */
	@Override
	public RespuestaApi refreshToken(String token) {
		RespuestaApi rpta = new RespuestaApi();
		rpta.setStatus("ERROR");
		rpta.setBody("No se pudo autenticar");

		Map<String, String> authParams = new HashMap<String, String>();
		authParams.put("REFRESH_TOKEN", token);

		/*
		 * try { InitiateAuthRequest authRequest = new InitiateAuthRequest()
		 * .withAuthFlow(AuthFlowType.REFRESH_TOKEN_AUTH)
		 * .withAuthParameters(authParams) .withClientId(cognitoClienteId);
		 * 
		 * InitiateAuthResult authResponse = cognitoClient.initiateAuth(authRequest);
		 * 
		 * if (authResponse.getChallengeName() == null ||
		 * authResponse.getChallengeName().isEmpty()) {
		 * authResponse.getAuthenticationResult().getAccessToken();
		 * rpta.setStatus("OK");
		 * rpta.setAccessToken(authResponse.getAuthenticationResult().getAccessToken());
		 * rpta.setIdToken(authResponse.getAuthenticationResult().getIdToken());
		 * rpta.setBody("actualizacion token correcta"); } } catch
		 * (NotAuthorizedException e) { rpta.setBody("Usuario no autorizado"); } catch
		 * (UserNotFoundException e) { rpta.setBody("Usuario no autorizado"); }
		 * catch(PasswordResetRequiredException e) {
		 * rpta.setBody("Reinicie su password"); rpta.setStatus("OK-RESET"); }
		 */

		return rpta;
	}

}
