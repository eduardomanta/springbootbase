package pe.gob.onpe.springboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.onpe.springboot.dao.iface.IEncuestaDao;
import pe.gob.onpe.springboot.model.Encuesta;
import pe.gob.onpe.springboot.service.iface.IEncuestaService;

@Service
public class EncuestaServiceImpl implements IEncuestaService {

	@Autowired
	IEncuestaDao encuestaDao;
	
	@Override
	public List<Encuesta> list() {
		return encuestaDao.list();
	}
	
	@Override
	public void save(Encuesta e) {
		encuestaDao.save(e);
	}

}
