package pe.gob.onpe.springboot.dto;

public class BasicAccessDTO {

	private String token;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
