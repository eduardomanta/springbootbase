package pe.gob.onpe.springboot.dao.iface;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import pe.gob.onpe.springboot.model.Encuesta;

@Repository
public interface IEncuestaDao  extends JpaRepository<Encuesta, Integer>  {
	@Query(value="SELECT N_ENCUESTA_PK,C_NOMBRES,C_APELLIDO_PATERNO,C_APELLIDO_MATERNO,N_EDAD,case  when C_CURSO='j' then 'Java' else 'C Sharp' end  as C_CURSO from TAB_ENCUESTA",nativeQuery=true)
	public List<Encuesta> list();
}
