package pe.gob.onpe.springboot.dao.iface;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import pe.gob.onpe.springboot.model.Encuesta;
import pe.gob.onpe.springboot.model.Usuario;

public interface IUsuarioDao extends JpaRepository<Usuario, Integer>  {
	@Query(value="SELECT N_USUARIO_PK,C_USUARIO,C_NOMBRES,C_APELLIDO_PATERNO,C_APELLIDO_MATERNO,C_PERFIL FROM TAB_USUARIO WHERE UPPER(C_USUARIO)=UPPER(?1) AND C_CLAVE=?2 AND ROWNUM=1",nativeQuery=true)
	public Usuario login(String usuario,String clave);
}
