package pe.gob.onpe.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TAB_USUARIO")
public class Usuario {

	@Id
	@Column(name = "N_USUARIO_PK")
	private Integer id;

	@Column(name = "C_USUARIO")
	private String usuario;

	@Column(name = "C_NOMBRES")
	private String nombres;

	@Column(name = "C_APELLIDO_PATERNO")
	private String apellidoPaterno;
	
	@Column(name = "C_APELLIDO_MATERNO")
	private String apellidoMaterno;
	
	@Column(name = "C_PERFIL")
	private String perfil;

	@Transient
	@Column(name = "C_CLAVE")
	private String clave;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
	
	

}
