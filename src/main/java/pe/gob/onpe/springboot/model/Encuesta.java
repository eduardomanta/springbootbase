
package pe.gob.onpe.springboot.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name="TAB_ENCUESTA")
public class Encuesta implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="encuestaSequence")
	@SequenceGenerator(name="encuestaSequence", sequenceName = "SEQ_ENCUESTA_PK", allocationSize=1)
	@Column(name="N_ENCUESTA_PK")
	private Integer id;
	@Column(name="C_NOMBRES")
	private String nombres;
	@Column(name="C_APELLIDO_PATERNO")
    private String apellidoPaterno;
	@Column(name="C_APELLIDO_MATERNO")
    private String apellidoMaterno;
	@Column(name="N_EDAD")
    private int edad;
	@Column(name="C_CURSO")
    private String curso;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
    
    
}
