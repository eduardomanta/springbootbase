package pe.gob.onpe.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;





@EnableTransactionManagement
@EnableJpaRepositories
@SpringBootApplication(exclude={ SecurityAutoConfiguration.class,ManagementWebSecurityAutoConfiguration.class})
@Import({SwaggerConfig.class/*,SecurityConfiguration.class*/})
public class EncuestaApplication {
	public static void main(String[] args) {
		SpringApplication.run(EncuestaApplication.class, args);
	}
	/*@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}*/
}

