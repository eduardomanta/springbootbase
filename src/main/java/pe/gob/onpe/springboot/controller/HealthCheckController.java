package pe.gob.onpe.springboot.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class HealthCheckController {

	@RequestMapping(method=RequestMethod.GET,path="/health")
	public String healthCheck() {
		return "OK";
	}
	
}
