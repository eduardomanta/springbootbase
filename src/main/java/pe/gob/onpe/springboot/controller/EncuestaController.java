package pe.gob.onpe.springboot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.gob.onpe.springboot.dto.RespuestaApi;
import pe.gob.onpe.springboot.model.Encuesta;
import pe.gob.onpe.springboot.service.iface.IEncuestaService;

@RestController
@CrossOrigin
@RequestMapping("api/encuesta")
public class EncuestaController {
 
	private static final Logger logger = LoggerFactory.getLogger(EncuestaController.class);
	@Autowired
	IEncuestaService encuestaService;
	
	@GetMapping(value="list",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Encuesta>> list(){
		try {
			return new ResponseEntity<List<Encuesta>>(encuestaService.list(), HttpStatus.OK);
		}catch(Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping(value="save",produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> save(@RequestBody Encuesta encuesta){
		try {
			encuestaService.save(encuesta);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""),HttpStatus.OK);
		} catch (Exception e) {
			logger.error("Error: ",e);
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
